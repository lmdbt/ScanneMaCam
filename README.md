# [ScanneMaCam](https://lmdbt.forge.apps.education.fr/ScanneMaCam/)

![](https://scannemacam.forge.apps.education.fr/capturegenerale.png)

[**ScanneMaCam** <b class="fa-solid fa-arrow-pointer"></b>](https://lmdbt.forge.apps.education.fr/ScanneMaCam/) est une application web conçue pour faciliter la capture d'images de cahiers d'élèves, de fiches d'activité, etc. Elle vise à simplifier le remplissage des cahiers de textes par les enseignants et à encourager les élèves à rattraper rapidement les cours manqués.

<div style="display:flex; justify-content:space-between; text-align:center; width:100%;">
  <img src="https://scannemacam.forge.apps.education.fr/capture1.png" style="width:30%; height:auto;">
  <img src="https://scannemacam.forge.apps.education.fr/capture2.png" style="width:30%; height:auto;">
  <img src="https://scannemacam.forge.apps.education.fr/capture3.png" style="width:30%; height:auto;">
</div>

## Fonctionnalités

1. **Prise de photo avec un visualiseur :** Utilisez votre visualiseur pour capturer une image du document souhaité directement depuis l'application.
2. **Recadrage de l'image :** Ajustez la zone de l'image pour ne conserver que la partie pertinente du document.
3. **Ajustement des paramètres d'image :** Modifiez la luminosité, le contraste et la saturation de l'image. Réglez la saturation à zéro pour obtenir une image en noir et blanc.
4. **Rotation et symétrie (image & vidéo) :** 
   - Faites pivoter l'image de 90° (sens horaire ou antihoraire) ou 180°, 270°, tout en conservant son ratio et sans rognage.  
   - Appliquez un effet miroir (symétrie horizontale ou verticale).  
   - Le flux vidéo peut lui aussi être pivoté de 90°, 180° ou 270° sans être rogné : la vidéo s'adaptera automatiquement à la fenêtre.
5. **Annuler le recadrage :** Si vous n'êtes pas satisfait du recadrage, revenez en arrière pour rétablir l'image originale et recadrez à nouveau.
6. **Copier l'image :** Copiez l'image traitée directement dans le presse-papiers pour une utilisation immédiate dans d'autres applications.
7. **Exportation :** Enregistrez l'image finale en format PNG ou PDF pour une utilisation ultérieure.
8. **Plein écran :** Cliquez sur le bouton "plein écran" pour basculer la vidéo (ou l'image recadrée et filtrée) en plein écran. Appuyez sur la touche `echap` du clavier pour  sortir du plein écran.

## Comment utiliser ScanneMaCam

### Prérequis

- Un navigateur web à jour (Firefox, Chrome, Brave, Edge, etc.).
- Un visualiseur connecté à votre ordinateur.

### Étapes

1. **Accéder à l'application :**  
   - Ouvrez la page de ScanneMaCam dans votre navigateur.  
   - L’interface n’affiche pas de barres de défilement (scroll) : la vidéo et les images sont automatiquement redimensionnées pour tenir dans la fenêtre.

2. **Autoriser l'accès à la webcam :**  
   - Lorsque le navigateur vous le demande, autorisez l'application à accéder à votre webcam.

3. **Prendre une photo :**  
   - Assurez-vous que le document est bien cadré dans la prévisualisation.  
   - Cliquez sur le bouton **"Prendre une photo"**.  
   - L’affichage bascule vers l’image capturée et ses outils de modification (réglages, recadrage, export, etc.).

4. **Recadrer l'image :**  
   - Utilisez les poignées du cadre pour sélectionner la zone souhaitée.  
   - Cliquez sur **"Recadrer"** pour appliquer les modifications.  
   - Si vous souhaitez annuler le recadrage, cliquez sur le bouton **"Annuler le recadrage"** (icône flèche vers la gauche) pour revenir à l'image originale.

5. **Ajuster les paramètres d'image :**  
   - Modifiez la **luminosité**, le **contraste** et la **saturation** via les curseurs.  
   - Pour une image en noir et blanc, réglez la **saturation** à **0**.  
   - Les modifications sont appliquées en temps réel.

6. **Appliquer des rotations et symétries :**  
   - **Rotation :** Utilisez les boutons pour faire pivoter l'image de **90°** (dans le sens horaire ou antihoraire), **180°** ou **270°**.  
   - **Symétrie :** Utilisez les boutons pour inverser l'image **horizontalement** ou **verticalement**.  
   - La vidéo d’aperçu peut aussi être **rotée** de la même façon pour mieux cadrer le document à la prise de vue.

7. **Copier ou exporter l'image :**  
   - **Copier l'image :** Cliquez sur **"Copier l'image"** pour copier l'image traitée dans le presse-papiers. Vous pourrez la coller dans un document, dans un cahier de texte numérique etc.  
   - **Exporter en Image :** Pour enregistrer en format PNG, cliquez sur **"Exporter en Image"**.  
   - **Exporter en PDF :** Pour enregistrer en format PDF, cliquez sur **"Exporter en PDF"**.  
   - Le fichier sera alors téléchargé sur votre appareil.

## Version Portable

Une version portable est disponible. Elle vous permet d'utiliser l'application sans connexion internet. Pour cela :

- Téléchargez l'archive dans le dépôt ou en [cliquant ici <b class="fa-solid fa-arrow-pointer"></b>](https://forge.apps.education.fr/scannemacam/scannemacam.forge.apps.education.fr/-/raw/main/ScanneMaCamPortable_fev2025.zip?ref_type=heads&inline=false).
- Décompressez l'archive dans un dossier sur votre machine (ou sur une clé USB).
- Ouvrez la page `lancerScanneMaCam.html` avec votre navigateur.
- Profitez de l'appli hors ligne !

## Librairies utilisées

- **[Cropper.js](https://github.com/fengyuanchen/cropperjs)** : Bibliothèque JavaScript pour le recadrage des images.
- **[jsPDF](https://github.com/parallax/jsPDF)** : Bibliothèque pour la génération de fichiers PDF.
- **[Font Awesome](https://fontawesome.com/)** : Ensemble d'icônes vectorielles pour embellir l'interface utilisateur.

## Licences

### ScanneMaCam

- **Licence :** MIT  
- **Auteurs :** Jean-Baptiste Clap - Cyril Iaconelli

### Bibliothèques tierces

1. **Cropper.js**
   - **Lien :** [https://github.com/fengyuanchen/cropperjs](https://github.com/fengyuanchen/cropperjs)
   - **Licence :** [MIT License](https://opensource.org/licenses/MIT)

2. **jsPDF**
   - **Lien :** [https://github.com/parallax/jsPDF](https://github.com/parallax/jsPDF)
   - **Licence :** [MIT License](https://opensource.org/licenses/MIT)

3. **Font Awesome**
   - **Lien :** [https://fontawesome.com/](https://fontawesome.com/)
   - **Licence :** [Creative Commons Attribution 4.0 International](https://fontawesome.com/license/free)

## Remerciements

Un grand merci à toutes les personnes et communautés qui ont contribué au développement des bibliothèques utilisées dans cette application.

## Notes supplémentaires

- **Confidentialité :** L'application n'enregistre aucune image sur un serveur. Toutes les opérations sont effectuées localement dans votre navigateur.
- **Compatibilité :** Pour des raisons de sécurité, certaines fonctionnalités comme l'accès au presse-papiers ou à la webcam nécessitent que la page soit servie via HTTPS ou depuis `localhost`.
- **Conseil :** Assurez-vous que votre navigateur est à jour pour profiter pleinement de toutes les fonctionnalités de ScanneMaCam.
