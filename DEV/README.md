# [ScanneMaCam](https://lmdbt.forge.apps.education.fr/ScanneMaCam/)

[**ScanneMaCam** <b class="fa-solid fa-arrow-pointer"></b>](https://lmdbt.forge.apps.education.fr/ScanneMaCam/) est une application web conçue pour faciliter la capture d'images de cahiers d'élèves, de fiches d'activité, etc. Elle vise à simplifier le remplissage des cahiers de textes par les enseignants et à encourager les élèves à rattraper rapidement les cours manqués.

<div style="display:flex; justify-content:space-between; text-align:center; width:100%;">
  <img src="https://lmdbt.forge.apps.education.fr/ScanneMaCam/Presentation1.png" style="width:30%; height:auto;">
  <img src="https://lmdbt.forge.apps.education.fr/ScanneMaCam/Presentation2.png" style="width:30%; height:auto;">
  <img src="https://lmdbt.forge.apps.education.fr/ScanneMaCam/Presentation3.png" style="width:30%; height:auto;">
</div>


## Fonctionnalités

1. **Prise de photo avec la webcam :** Utilisez votre webcam pour capturer une image du document souhaité directement depuis l'application.
2. **Recadrage de l'image :** Ajustez la zone de l'image pour ne conserver que la partie pertinente du document.
3. **Ajustement des paramètres d'image :** Modifiez la luminosité, le contraste et la saturation de l'image. Réglez la saturation à zéro pour obtenir une image en noir et blanc.
4. **Rotation et symétrie :** Faites pivoter l'image de 90° dans le sens horaire ou antihoraire. Appliquez des miroirs horizontaux et verticaux pour obtenir la symétrie souhaitée.
5. **Annuler le recadrage :** Si vous n'êtes pas satisfait du recadrage, revenez en arrière pour rétablir l'image originale et recadrez à nouveau.
6. **Copier l'image :** Copiez l'image traitée directement dans le presse-papiers pour une utilisation immédiate dans d'autres applications.
7. **Exportation :** Enregistrez l'image finale en format PNG ou PDF pour une utilisation ultérieure.

## Comment utiliser ScanneMaCam

### Prérequis

- Un navigateur web à jour (Firefox, Chrome, Brave, Edge, etc.).
- Une webcam (visualiseuse) fonctionnelle connectée à votre ordinateur.

### Étapes

1. **Accéder à l'application :** Ouvrez la page de ScanneMaCam dans votre navigateur. La page défilera automatiquement pour centrer la fenêtre vidéo verticalement sur votre écran.
2. **Autoriser l'accès à la webcam :** Lorsque le navigateur vous le demande, autorisez l'application à accéder à votre webcam.
3. **Prendre une photo :**
   - Assurez-vous que le document est bien cadré dans la fenêtre de prévisualisation.
   - Cliquez sur le bouton **"Prendre une photo"**.
   - La page défilera automatiquement vers l'image capturée et les outils de modification.
4. **Recadrer l'image :**
   - Utilisez les poignées du cadre pour sélectionner la zone souhaitée.
   - Cliquez sur **"Recadrer"** pour appliquer les modifications.
   - Si vous souhaitez annuler le recadrage, cliquez sur le bouton **"Annuler le recadrage"** (icône flèche vers la gauche) pour revenir à l'image originale.
5. **Ajuster les paramètres d'image :**
   - Utilisez les curseurs pour modifier la **luminosité**, le **contraste** et la **saturation**.
   - Pour une image en noir et blanc, réglez la **saturation** à **0**.
   - Les modifications sont appliquées en temps réel.
6. **Appliquer des rotations et symétries :**
   - **Rotation :** Utilisez les boutons pour faire pivoter l'image de **90°** dans le sens horaire ou antihoraire.
   - **Symétrie :** Utilisez les boutons pour appliquer une symétrie **horizontale** ou **verticale** à l'image.
7. **Copier ou exporter l'image :**
   - **Copier l'image :** Cliquez sur **"Copier l'image"** pour copier l'image traitée dans le presse-papiers. Vous pouvez ensuite la coller dans une autre application (par exemple, un document Word ou un e-mail).
   - **Exporter en Image :** Pour enregistrer en format PNG, cliquez sur **"Exporter en Image"**.
   - **Exporter en PDF :** Pour enregistrer en format PDF, cliquez sur **"Exporter en PDF"**.
   - Le fichier sera téléchargé automatiquement sur votre appareil.

## Librairies utilisées

- **[Cropper.js](https://github.com/fengyuanchen/cropperjs)** : Bibliothèque JavaScript pour le recadrage des images.
- **[jsPDF](https://github.com/parallax/jsPDF)** : Bibliothèque pour la génération de fichiers PDF.
- **[Font Awesome](https://fontawesome.com/)** : Ensemble d'icônes vectorielles pour embellir l'interface utilisateur.

## Licences

### ScanneMaCam

- **Licence :** [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
- **Auteur :** Cyril Iaconelli

### Bibliothèques tierces

1. **Cropper.js**
   - **Lien :** [https://github.com/fengyuanchen/cropperjs](https://github.com/fengyuanchen/cropperjs)
   - **Licence :** [MIT License](https://opensource.org/licenses/MIT)

2. **jsPDF**
   - **Lien :** [https://github.com/parallax/jsPDF](https://github.com/parallax/jsPDF)
   - **Licence :** [MIT License](https://opensource.org/licenses/MIT)

3. **Font Awesome**
   - **Lien :** [https://fontawesome.com/](https://fontawesome.com/)
   - **Licence :** [Creative Commons Attribution 4.0 International](https://fontawesome.com/license/free)

## Remerciements

Un grand merci à toutes les personnes et communautés qui ont contribué au développement des bibliothèques utilisées dans cette application.

## Notes supplémentaires

- **Confidentialité :** L'application n'enregistre aucune image sur un serveur. Toutes les opérations sont effectuées localement dans votre navigateur.
- **Compatibilité :** Pour des raisons de sécurité, certaines fonctionnalités comme l'accès au presse-papiers ou à la webcam nécessitent que la page soit servie via HTTPS ou depuis `localhost`.
- **Conseil :** Assurez-vous que votre navigateur est à jour pour profiter pleinement de toutes les fonctionnalités de ScanneMaCam.
