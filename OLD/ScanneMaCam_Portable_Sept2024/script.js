// script.js

// Accès aux éléments du DOM
const video = document.getElementById('video');
const canvas = document.getElementById('canvas');
const captureButton = document.getElementById('capture');
const cropButton = document.getElementById('crop');
const undoCropButton = document.getElementById('undoCrop');
const rotateLeftButton = document.getElementById('rotateLeft');
const rotateRightButton = document.getElementById('rotateRight');
const symHorizButton = document.getElementById('symHoriz'); // Nouveau bouton pour miroir horizontal
const symVertButton = document.getElementById('symVert');   // Nouveau bouton pour miroir vertical
const brightnessSlider = document.getElementById('brightness');
const contrastSlider = document.getElementById('contrast');
const saturationSlider = document.getElementById('saturation');
const exportImageButton = document.getElementById('exportImage');
const exportPDFButton = document.getElementById('exportPDF');
const copyImageButton = document.getElementById('copyImage');

let cropper;
let originalImage = null;
let preCropImage = null;
let rotationAngle = 0;
let flipHorizontal = 1; // Variable pour le miroir horizontal (1 ou -1)
let flipVertical = 1;   // Variable pour le miroir vertical (1 ou -1)

// Demander l'accès à la webcam avec une résolution maximale
navigator.mediaDevices.getUserMedia({ video: { width: { ideal: 1920 }, height: { ideal: 1080 } } })
.then(stream => {
    video.srcObject = stream;
})
.catch(err => {
    console.error('Erreur d\'accès à la webcam:', err);
});

// Ajouter un écouteur pour l'événement 'load' de la fenêtre
window.addEventListener('load', () => {
    // Obtenir la hauteur de la fenêtre
    const windowHeight = window.innerHeight;
    // Obtenir la position du 'camera' par rapport au document
    const cameraElement = document.getElementById('camera');
    const cameraRect = cameraElement.getBoundingClientRect();
    const cameraTop = cameraRect.top + window.pageYOffset;
    const cameraHeight = cameraRect.height;
    // Calculer la position pour centrer le 'camera' verticalement
    const scrollToPosition = cameraTop - (windowHeight / 2) + (cameraHeight / 2);
    // Faire défiler la page jusqu'à cette position avec un comportement doux
    window.scrollTo({
        top: scrollToPosition,
        behavior: 'smooth'
    });
});

// Capturer l'image
captureButton.addEventListener('click', () => {
    const context = canvas.getContext('2d');
    canvas.width = video.videoWidth;
    canvas.height = video.videoHeight;
    context.drawImage(video, 0, 0, canvas.width, canvas.height);
    originalImage = new Image();
    originalImage.src = canvas.toDataURL();
    canvas.style.display = 'block';

    // Réinitialiser les transformations
    rotationAngle = 0;
    flipHorizontal = 1;
    flipVertical = 1;

    // Désactiver les boutons et curseurs
    rotateLeftButton.disabled = true;
    rotateRightButton.disabled = true;
    symHorizButton.disabled = true;
    symVertButton.disabled = true;
    brightnessSlider.disabled = true;
    contrastSlider.disabled = true;
    saturationSlider.disabled = true;
    copyImageButton.disabled = true;
    undoCropButton.disabled = true;

    // Faire défiler la page vers l'image et les curseurs
    document.getElementById('snapshot').scrollIntoView({ behavior: 'smooth' });

    if (cropper) {
        cropper.destroy();
    }
    cropper = new Cropper(canvas, {
        aspectRatio: NaN,
        viewMode: 1,
    });
});

// Recadrer l'image
cropButton.addEventListener('click', () => {
    if (cropper) {
        // Sauvegarder l'image originale avant le recadrage
        preCropImage = originalImage;

        const croppedCanvas = cropper.getCroppedCanvas();
        canvas.width = croppedCanvas.width;
        canvas.height = croppedCanvas.height;
        const context = canvas.getContext('2d');
        context.clearRect(0, 0, canvas.width, canvas.height);
        context.drawImage(croppedCanvas, 0, 0);

        cropper.destroy();
        cropper = null;

        // Mettre à jour l'image originale
        originalImage = new Image();
        originalImage.onload = function() {
            // Appliquer les filtres après que l'image soit chargée
            applyFilters();
        };
        originalImage.src = canvas.toDataURL();

        // Activer les boutons et curseurs
        rotateLeftButton.disabled = false;
        rotateRightButton.disabled = false;
        symHorizButton.disabled = false;
        symVertButton.disabled = false;
        brightnessSlider.disabled = false;
        contrastSlider.disabled = false;
        saturationSlider.disabled = false;
        copyImageButton.disabled = false;
        undoCropButton.disabled = false;
    }
});

// Annuler le recadrage
undoCropButton.addEventListener('click', () => {
    if (preCropImage) {
        originalImage = preCropImage;
        canvas.width = originalImage.width;
        canvas.height = originalImage.height;
        const context = canvas.getContext('2d');
        context.clearRect(0, 0, canvas.width, canvas.height);

        // Réinitialiser les transformations
        rotationAngle = 0;
        flipHorizontal = 1;
        flipVertical = 1;

        // Afficher l'image originale sans recadrage
        originalImage.onload = function() {
            context.drawImage(originalImage, 0, 0, canvas.width, canvas.height);

            // Réinitialiser les curseurs
            brightnessSlider.value = 0;
            contrastSlider.value = 0;
            saturationSlider.value = 0;

            // Désactiver les boutons et curseurs
            rotateLeftButton.disabled = true;
            rotateRightButton.disabled = true;
            symHorizButton.disabled = true;
            symVertButton.disabled = true;
            brightnessSlider.disabled = true;
            contrastSlider.disabled = true;
            saturationSlider.disabled = true;
            copyImageButton.disabled = true;
            undoCropButton.disabled = true;

            // Réinitialiser les filtres
            context.filter = 'none';

            // Recréer le cropper pour permettre de recadrer à nouveau
            if (cropper) {
                cropper.destroy();
            }
            cropper = new Cropper(canvas, {
                aspectRatio: NaN,
                viewMode: 1,
            });
        };
        originalImage.src = preCropImage.src;

        // Supprimer preCropImage pour éviter plusieurs annulations successives
        preCropImage = null;
    }
});

// Fonction pour appliquer les filtres et les transformations
function applyFilters() {
    if (!originalImage) return;
    const brightness = parseInt(brightnessSlider.value);
    const contrast = parseInt(contrastSlider.value);
    const saturation = parseInt(saturationSlider.value);
    const context = canvas.getContext('2d');

    // Effacer le canvas
    context.clearRect(0, 0, canvas.width, canvas.height);

    // Sauvegarder le contexte avant les transformations
    context.save();

    // Déplacer le contexte au centre de l'image pour les transformations
    context.translate(canvas.width / 2, canvas.height / 2);

    // Appliquer la rotation
    context.rotate((rotationAngle * Math.PI) / 180);

    // Appliquer les symétries
    context.scale(flipHorizontal, flipVertical);

    // Définir les filtres
    context.filter = `brightness(${brightness + 100}%) contrast(${contrast + 100}%) saturate(${saturation + 100}%)`;

    // Dessiner l'image avec les transformations appliquées
    context.drawImage(
        originalImage,
        -originalImage.width / 2,
        -originalImage.height / 2,
        originalImage.width,
        originalImage.height
    );

    // Restaurer le contexte à son état initial
    context.restore();

    // Réinitialiser le filtre
    context.filter = 'none';
}

// Écouteurs pour les curseurs
brightnessSlider.addEventListener('input', applyFilters);
contrastSlider.addEventListener('input', applyFilters);
saturationSlider.addEventListener('input', applyFilters);

// Écouteurs pour les boutons de rotation
rotateLeftButton.addEventListener('click', () => {
    rotationAngle = (rotationAngle - 90) % 360;
    adjustCanvasSize();
    applyFilters();
});

rotateRightButton.addEventListener('click', () => {
    rotationAngle = (rotationAngle + 90) % 360;
    adjustCanvasSize();
    applyFilters();
});

// **Écouteurs pour les boutons de symétrie**
symHorizButton.addEventListener('click', () => {
    flipVertical *= -1; // Inverser la valeur pour effectuer le miroir vertical
    applyFilters();
});

symVertButton.addEventListener('click', () => {
    flipHorizontal *= -1; // Inverser la valeur pour effectuer le miroir horizontal
    applyFilters();
});

// Fonction pour ajuster la taille du canvas après rotation
function adjustCanvasSize() {
    // Inverser largeur et hauteur si nécessaire
    if (rotationAngle % 180 !== 0) {
        [canvas.width, canvas.height] = [canvas.height, canvas.width];
    } else {
        canvas.width = originalImage.width;
        canvas.height = originalImage.height;
    }
}

// Exporter l'image en PNG
exportImageButton.addEventListener('click', () => {
    const link = document.createElement('a');
    link.download = 'image.png';
    link.href = canvas.toDataURL('image/png');
    link.click();
});

// Copier l'image dans le presse-papiers
copyImageButton.addEventListener('click', async () => {
    try {
        // Convertir le canvas en Blob
        canvas.toBlob(async (blob) => {
            const item = new ClipboardItem({ 'image/png': blob });
            await navigator.clipboard.write([item]);
            alert('Image copiée dans le presse-papiers !');
        }, 'image/png');
    } catch (err) {
        console.error('Erreur lors de la copie de l\'image :', err);
        alert('Erreur lors de la copie de l\'image. Veuillez réessayer.');
    }
});

// Exporter l'image en PDF
exportPDFButton.addEventListener('click', () => {
    const { jsPDF } = window.jspdf;
    const pdf = new jsPDF();
    const imgData = canvas.toDataURL('image/png');
    const imgWidth = canvas.width;
    const imgHeight = canvas.height;
    // Ajuster la taille de l'image dans le PDF si nécessaire
    const pdfWidth = pdf.internal.pageSize.getWidth();
    const pdfHeight = (imgHeight * pdfWidth) / imgWidth;
    pdf.addImage(imgData, 'PNG', 0, 0, pdfWidth, pdfHeight);
    pdf.save('document.pdf');
});
