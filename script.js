// ===========================
// Variables et DOM
// ===========================

// DOM
const video = document.getElementById('video');
const canvas = document.getElementById('canvas');
const captureButton = document.getElementById('capture');
const restartVideoButton = document.getElementById('restartVideo');
const cropButton = document.getElementById('crop');
const undoCropButton = document.getElementById('undoCrop');
const rotateLeftButton = document.getElementById('rotateLeft');
const rotateRightButton = document.getElementById('rotateRight');
const symHorizButton = document.getElementById('symHoriz');
const symVertButton = document.getElementById('symVert');
const flipHorizButton = document.getElementById('flipHorizButton');
const flipVertButton = document.getElementById('flipVertButton');
const rotateVideoLeftButton = document.getElementById('rotateVideoLeftButton');
const rotateVideoRightButton = document.getElementById('rotateVideoRightButton');
const brightnessSlider = document.getElementById('brightness');
const contrastSlider = document.getElementById('contrast');
const saturationSlider = document.getElementById('saturation');
const sharpnessSlider = document.getElementById('sharpness');
const exportImageButton = document.getElementById('exportImage');
const exportPDFButton = document.getElementById('exportPDF');
const copyImageButton = document.getElementById('copyImage');
const toggleControlsButton = document.getElementById('toggleControlsButton');
const controlsDiv = document.getElementById('controls');
const toggleFullscreenVideoButton = document.getElementById('toggleFullscreenVideoButton');
const cameraContainer = document.getElementById('cameraContainer'); // le conteneur de la vidéo

let cropper = null;

// Image « brute » sur laquelle on applique les filtres
let originalImage = null;

// On introduit une variable pour la toute première capture
let firstCapturedImage = null;

// Transformations sur l'image (rotation, flips)
let rotationAngle = 0;
let flipHorizontal = 1;
let flipVertical = 1;

// Transformations sur la vidéo
let videoRotationAngle = 0;
let videoFlipHorizontal = 1;
let videoFlipVertical = 1;

// Affichage des contrôles
let controlsVisible = true;

// ===========================
// 1) Gestion de la Webcam
// ===========================

function startVideo() {
  const constraints = {
    video: { width: { ideal: 1920 }, height: { ideal: 1080 } }
  };

  navigator.mediaDevices.getUserMedia(constraints)
    .then(stream => {
      video.srcObject = stream;
      video.style.display = 'block';
      canvas.style.display = 'none';
      controlsDiv.style.display = 'none';
      applyVideoTransforms();

      captureButton.disabled = false;
      restartVideoButton.disabled = true;
    })
    .catch(err => {
      console.error('Erreur d\'accès à la webcam:', err);
    });
}
startVideo();

function applyVideoTransforms() {
  video.style.transform = `rotate(${videoRotationAngle}deg) scale(${videoFlipHorizontal}, ${videoFlipVertical})`;
}

// Écouteurs rotation/symétrie vidéo
rotateVideoLeftButton.addEventListener('click', () => {
  videoRotationAngle = (videoRotationAngle - 90) % 360;
  applyVideoTransforms();
});
rotateVideoRightButton.addEventListener('click', () => {
  videoRotationAngle = (videoRotationAngle + 90) % 360;
  applyVideoTransforms();
});
flipHorizButton.addEventListener('click', () => {
  videoFlipHorizontal *= -1;
  applyVideoTransforms();
});
flipVertButton.addEventListener('click', () => {
  videoFlipVertical *= -1;
  applyVideoTransforms();
});

// Permettre un clic sur la vidéo pour capturer
video.addEventListener('click', () => {
  captureButton.click();
});

// ===========================
// Appliquer des filtres sur la vidéo
// ===========================
const brightness4cam = document.getElementById('brightness4cam');
const contrast4cam   = document.getElementById('contrast4cam');
const saturation4cam = document.getElementById('saturation4cam');

function applyVideoFilters() {
  const brightnessVal = parseInt(brightness4cam.value, 10);
  const contrastVal   = parseInt(contrast4cam.value, 10);
  const saturationVal = parseInt(saturation4cam.value, 10);
  
  // On ajoute 100 pour partir de 100% (0 => 100%)
  video.style.filter = `brightness(${brightnessVal + 100}%) contrast(${contrastVal + 100}%) saturate(${saturationVal + 100}%)`;
}

// Écouteurs pour mettre à jour le filtre dès que l'utilisateur modifie un slider
brightness4cam.addEventListener('input', applyVideoFilters);
contrast4cam.addEventListener('input', applyVideoFilters);
saturation4cam.addEventListener('input', applyVideoFilters);

// ===========================
// PLEIN ÉCRAN
// ===========================

function enterFullscreenVideo() {
  if (cameraContainer.requestFullscreen) {
    cameraContainer.requestFullscreen();
  } else if (cameraContainer.webkitRequestFullscreen) {  // Safari
    cameraContainer.webkitRequestFullscreen();
  } else if (cameraContainer.msRequestFullscreen) {      // IE11
    cameraContainer.msRequestFullscreen();
  }
}

// Quand on clique sur le bouton
toggleFullscreenVideoButton.addEventListener('click', () => {
  enterFullscreenVideo();
});



// ===========================
// 2) Capture
// ===========================

captureButton.addEventListener('click', () => {
  const ctx = canvas.getContext('2d');

  // Récupérer dimensions de la vidéo
  const videoWidth = video.videoWidth;
  const videoHeight = video.videoHeight;

  // Ajuster le canvas selon la rotation vidéo
  if (videoRotationAngle % 180 !== 0) {
    canvas.width = videoHeight;
    canvas.height = videoWidth;
  } else {
    canvas.width = videoWidth;
    canvas.height = videoHeight;
  }

  ctx.save();
  ctx.translate(canvas.width / 2, canvas.height / 2);
  ctx.rotate((videoRotationAngle * Math.PI) / 180);
  ctx.scale(videoFlipHorizontal, videoFlipVertical);
  
  // Appliquer les filtres (issus de la vidéo)
  ctx.filter = video.style.filter;
  
  ctx.drawImage(video, -videoWidth / 2, -videoHeight / 2, videoWidth, videoHeight);
  ctx.restore();

  // Stocker la capture dans originalImage
  originalImage = new Image();
  originalImage.src = canvas.toDataURL();

  // Stocker la toute première image pour Undo
  firstCapturedImage = new Image();
  firstCapturedImage.src = originalImage.src;

  // Affichage
  video.style.display = 'none';
  canvas.style.display = 'block';

  // Arrêter la webcam
  const stream = video.srcObject;
  if (stream) {
    stream.getTracks().forEach(track => track.stop());
  }
  video.srcObject = null;

  captureButton.disabled = true;
  restartVideoButton.disabled = false;

  // Réinit transformations image
  rotationAngle = 0;
  flipHorizontal = 1;
  flipVertical = 1;

  // Désactiver curseurs/boutons (jusqu'à un crop)
  rotateLeftButton.disabled = true;
  rotateRightButton.disabled = true;
  symHorizButton.disabled = true;
  symVertButton.disabled = true;
  brightnessSlider.disabled = true;
  contrastSlider.disabled = true;
  saturationSlider.disabled = true;
  sharpnessSlider.disabled = true;
  copyImageButton.disabled = true;
  exportPDFButton.disabled = true;
  exportImageButton.disabled = true;
  cropButton.disabled = false;
  undoCropButton.disabled = true;
  // Boutons de contrôle vidéo 
  rotateVideoLeftButton.disabled = true;
  rotateVideoRightButton.disabled = true;
  flipVertButton.disabled = true;
  flipHorizButton.disabled = true;
  
  document.getElementById('sliders4cam').style.display = 'none';
  
  // Scroll vers les contrôles
  controlsDiv.scrollIntoView({ behavior: 'smooth' });

  // Initialiser le Cropper
  if (cropper) {
    cropper.destroy();
  }
  cropper = new Cropper(canvas, { aspectRatio: NaN, viewMode: 1 });
});

// Relancer la webcam
restartVideoButton.addEventListener('click', () => {
  startVideo();
  captureButton.disabled = false;
  restartVideoButton.disabled = true;
  undoCropButton.disabled = true;

  videoRotationAngle = 0;
  videoFlipHorizontal = 1;
  videoFlipVertical = 1;
  applyVideoTransforms();

  if (cropper) {
    cropper.destroy();
    cropper = null;
  }
  canvas.style.display = 'none';
  
  // On réactive les boutons de contrôle vidéo et on desactive le crop
  rotateVideoLeftButton.disabled = false;
  rotateVideoRightButton.disabled = false;
  flipVertButton.disabled = false;
  flipHorizButton.disabled = false;
  cropButton.disabled = true;
  
  document.getElementById('sliders4cam').style.display = 'block';
});

// ===========================
// 3) Recadrage & Undo
// ===========================

cropButton.addEventListener('click', () => {
  if (!cropper) return;
  
  controlsDiv.style.display = 'block';
  
  // Récupérer la zone recadrée
  const croppedCanvas = cropper.getCroppedCanvas();
  canvas.width = croppedCanvas.width;
  canvas.height = croppedCanvas.height;

  const ctx = canvas.getContext('2d');
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  ctx.drawImage(croppedCanvas, 0, 0);

  cropper.destroy();
  cropper = null;

  // Mettre à jour l'image courante
  originalImage = new Image();
  originalImage.src = canvas.toDataURL();

  originalImage.onload = () => {
    applyFilters(); // Réapplique (et réaffiche) avec les curseurs
  };

  // Réactiver les outils
  rotateLeftButton.disabled = false;
  rotateRightButton.disabled = false;
  symHorizButton.disabled = false;
  symVertButton.disabled = false;
  brightnessSlider.disabled = false;
  contrastSlider.disabled = false;
  saturationSlider.disabled = false;
  sharpnessSlider.disabled = false;
  copyImageButton.disabled = false;
  exportPDFButton.disabled = false;
  exportImageButton.disabled = false;
  undoCropButton.disabled = false;
  cropButton.disabled = true;
});

undoCropButton.addEventListener('click', () => {
  if (!firstCapturedImage) return;

  controlsDiv.style.display = 'none';
  
  // Désactiver de nouveau
  rotateLeftButton.disabled = true;
  rotateRightButton.disabled = true;
  symHorizButton.disabled = true;
  symVertButton.disabled = true;
  brightnessSlider.disabled = true;
  contrastSlider.disabled = true;
  saturationSlider.disabled = true;
  sharpnessSlider.disabled = true;
  copyImageButton.disabled = true;
  exportPDFButton.disabled = true;
  exportImageButton.disabled = true;
  undoCropButton.disabled = true;
  cropButton.disabled = false;

  // 1) Replacer originalImage = firstCapturedImage
  originalImage = firstCapturedImage;

  // 2) Réinitialiser les curseurs
  brightnessSlider.value = 0;
  contrastSlider.value = 0;
  saturationSlider.value = 0;
  sharpnessSlider.value = 1;

  // 3) On redessine => image d'origine + filtres "nuls"
  applyFilters();

  // Petit hack sur la netteté
  setTimeout(() => {
    let currentVal = parseInt(sharpnessSlider.value, 10);
    if (currentVal === 0) {
      currentVal = 1;
    } else {
      currentVal -= 1;
    }
    sharpnessSlider.value = currentVal;
    applyFilters();
  }, 1);

  // 4) Réinitialiser transformations
  rotationAngle = 0;
  flipHorizontal = 1;
  flipVertical = 1;

  // 5) Recalibre le canvas
  canvas.width = originalImage.width;
  canvas.height = originalImage.height;

  // 6) On réactive le cropper
  if (cropper) {
    cropper.destroy();
  }
  cropper = new Cropper(canvas, { aspectRatio: NaN, viewMode: 1 });

  cropButton.disabled = false;
  undoCropButton.disabled = true;
});

// ===========================
// 4) Filtres
// ===========================

function applyFilters() {
  if (!originalImage) return;

  const ctx = canvas.getContext('2d');

  // Dimensions RÉELLES de l'image source
  const realW = originalImage.width;
  const realH = originalImage.height;

  // Avant de dessiner, on ajuste la taille du canvas
  // selon la rotation de l'image (0°, 90°, 180°, 270°)
  if (rotationAngle % 180 === 0) {
    // 0° ou 180° => on garde (realW x realH)
    canvas.width = realW;
    canvas.height = realH;
  } else {
    // 90° ou 270° => on inverse
    canvas.width = realH;
    canvas.height = realW;
  }

  // Effacer avant de dessiner
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  ctx.save();

  // Centrage puis rotation + flip
  ctx.translate(canvas.width / 2, canvas.height / 2);
  ctx.rotate((rotationAngle * Math.PI) / 180);
  ctx.scale(flipHorizontal, flipVertical);

  // Filtres CSS
  const brightness = parseInt(brightnessSlider.value, 10);
  const contrast   = parseInt(contrastSlider.value, 10);
  const saturation = parseInt(saturationSlider.value, 10);
  ctx.filter = `brightness(${brightness + 100}%)
                contrast(${contrast + 100}%)
                saturate(${saturation + 100}%)`;

  // Dessin de l'image avec ses dimensions "réelles"
  ctx.drawImage(originalImage, -realW / 2, -realH / 2, realW, realH);
  ctx.restore();

  // Netteté
  const sharpenValue = parseInt(sharpnessSlider.value, 10);
  if (sharpenValue > 0) {
    applySharpen(ctx, canvas.width, canvas.height, sharpenValue);
  }

  // Mise à jour du cropper si actif
  if (cropper) {
    cropper.replace(canvas.toDataURL());
  }
}

// Liste d'écouteurs pour les sliders
brightnessSlider.addEventListener('input', applyFilters);
contrastSlider.addEventListener('input', applyFilters);
saturationSlider.addEventListener('input', applyFilters);
sharpnessSlider.addEventListener('input', applyFilters);

// Écouteurs pour la rotation/flips (image)
rotateLeftButton.addEventListener('click', () => {
  rotationAngle = (rotationAngle - 90) % 360;
  applyFilters();
});
rotateRightButton.addEventListener('click', () => {
  rotationAngle = (rotationAngle + 90) % 360;
  applyFilters();
});
symHorizButton.addEventListener('click', () => {
  flipVertical *= -1;
  applyFilters();
});
symVertButton.addEventListener('click', () => {
  flipHorizontal *= -1;
  applyFilters();
});

// ===========================
// 5) Export et copie
// ===========================

exportImageButton.addEventListener('click', () => {
  const link = document.createElement('a');
  link.download = 'image.png';
  link.href = canvas.toDataURL('image/png');
  link.click();
});

copyImageButton.addEventListener('click', async () => {
  try {
    canvas.toBlob(async (blob) => {
      const item = new ClipboardItem({ 'image/png': blob });
      await navigator.clipboard.write([item]);
      alert('Image copiée dans le presse-papiers !');
    }, 'image/png');
  } catch (err) {
    console.error('Erreur copie image :', err);
    alert('Erreur lors de la copie. Veuillez réessayer.');
  }
});

exportPDFButton.addEventListener('click', () => {
  const { jsPDF } = window.jspdf;
  const pdf = new jsPDF();
  const imgData = canvas.toDataURL('image/png');
  const w = canvas.width;
  const h = canvas.height;
  const pdfWidth = pdf.internal.pageSize.getWidth();
  const pdfHeight = (h * pdfWidth) / w;
  pdf.addImage(imgData, 'PNG', 0, 0, pdfWidth, pdfHeight);
  pdf.save('document.pdf');
});

// ===========================
// 6) Masquage des contrôles
// ===========================
toggleControlsButton.addEventListener('click', () => {
  controlsVisible = !controlsVisible;
  if (controlsVisible) {
    controlsDiv.style.display = 'block';
    toggleControlsButton.innerHTML = '<i class="fas fa-eye-slash"></i> Masquer les contrôles';
  } else {
    controlsDiv.style.display = 'none';
    toggleControlsButton.innerHTML = '<i class="fas fa-eye"></i> Afficher les contrôles';
  }
});

// ===========================
// 7) Filtre de netteté
// ===========================
function applySharpen(ctx, width, height, amount) {
  if (amount <= 0) return;

  const imageData = ctx.getImageData(0, 0, width, height);
  const data = imageData.data;
  const alpha = amount / 100;

  const kernel = [
    0,     -alpha,     0,
    -alpha, 1 + 4*alpha, -alpha,
    0,     -alpha,     0
  ];
  const output = new Uint8ClampedArray(data.length);

  for (let y = 1; y < height - 1; y++) {
    for (let x = 1; x < width - 1; x++) {
      const idx = (y * width + x) * 4;
      let r = 0, g = 0, b = 0, a = 0;
      let i = 0;

      for (let ky = -1; ky <= 1; ky++) {
        for (let kx = -1; kx <= 1; kx++) {
          const px = x + kx;
          const py = y + ky;
          const idxN = (py * width + px) * 4;
          const kVal = kernel[i++];
          r += data[idxN]     * kVal;
          g += data[idxN + 1] * kVal;
          b += data[idxN + 2] * kVal;
          a += data[idxN + 3] * kVal; 
        }
      }

      output[idx]   = Math.min(Math.max(r, 0), 255);
      output[idx+1] = Math.min(Math.max(g, 0), 255);
      output[idx+2] = Math.min(Math.max(b, 0), 255);
      output[idx+3] = Math.min(Math.max(a, 0), 255);
    }
  }
  // Gestion des bords (on recopie tel quel)
  for (let y = 0; y < height; y++) {
    for (let x = 0; x < width; x++) {
      if (x === 0 || x === width - 1 || y === 0 || y === height - 1) {
        const idx = (y * width + x) * 4;
        output[idx]   = data[idx];
        output[idx+1] = data[idx+1];
        output[idx+2] = data[idx+2];
        output[idx+3] = data[idx+3];
      }
    }
  }

  // On réinjecte dans data
  for (let i = 0; i < data.length; i++) {
    data[i] = output[i];
  }
  ctx.putImageData(imageData, 0, 0);
}
